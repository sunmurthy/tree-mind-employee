package com.tree.controller;

import java.io.IOException;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.tree.beans.Employee;
import com.tree.helper.EmployeeDTO;
import com.tree.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService empService;

	@PostMapping()
	public String createEmployees(@RequestBody EmployeeDTO employeeDTO )
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException,IllegalArgumentException {

		return empService.createEmployees(employeeDTO.getEmployeeList());

	}

	@GetMapping()
	public EmployeeDTO getEmployeesList() {
		EmployeeDTO response = new EmployeeDTO();
		 response.setEmployeeList(empService.getEmployeesList());
		 return response;
	}
	
	@GetMapping("/findBy")
	public EmployeeDTO findEmployeeByName(@RequestParam(value = "employee_name") String employeeName) {
		EmployeeDTO response = new EmployeeDTO();
		 response.setEmployeeList(empService.findEmployeeByName(employeeName));
		 return response;
	}
	
	@GetMapping("/{employee_id}")
	public Employee findEmployeeById(@PathVariable(value = "employee_id") long employeeId) {
		 return  empService.findEmployeeById(employeeId);
	}
	
	
	@PutMapping()
	public String updateEmployee(@RequestBody Employee employe)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {

		return empService.updateEmployee(employe);

	}

	@DeleteMapping()
	public String deleteEmployee(@RequestBody Employee employe) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {

		return empService.deleteEmployee(employe);

	}

}

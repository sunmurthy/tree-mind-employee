package com.tree.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.tree.beans.Employee;
import com.tree.helper.CsvFileOperationHelper;

@Service
public class EmployeeService {

	private CsvFileOperationHelper csv;

	public String createEmployees(List<Employee> employeeList)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException, IllegalArgumentException {
		csv = new CsvFileOperationHelper();
		List<Employee> existingEmpList = csv.readFromCsvFile();

		try {
			if (!(employeeList.isEmpty() || employeeList.size() == 0)) {
				if (!(existingEmpList.isEmpty() || existingEmpList.size() == 0)) {
					Set<Employee> employeeSet = new HashSet<Employee>(existingEmpList);
					employeeList.forEach(e -> {

						if (!employeeSet.add(e)) {
							throw new IllegalArgumentException(
									e.getEmpId() + " this employee already exist in the DB...!!!");
						}

					});

					employeeList.addAll(existingEmpList);
				}

			}

			csv.insertToCsvFile(employeeList);
			return "successfully inserted the employees";
		} catch (IllegalArgumentException ex) {
			return ex.getMessage();
		}

	}

	public List<Employee> getEmployeesList() {
		csv = new CsvFileOperationHelper();
		return csv.readFromCsvFile();

	}

	public String updateEmployee(Employee employe)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {
		csv = new CsvFileOperationHelper();

		String msg = "Sorry issue in updating the employee, Employee Does not Exist in DB  !!!";

		List<Employee> employeeList = csv.readFromCsvFile();

		if (!(employeeList.isEmpty() || employeeList.size() == 0)) {

			try {
				int index = employeeList.indexOf(employe);

				if (employeeList.set(index, employe) != null) {
					csv.insertToCsvFile(employeeList);
					msg = "Successfully updated the employee";
				}
			} catch (ArrayIndexOutOfBoundsException ex) {
				System.out.println(ex.getMessage());
			}
		}

		return msg;

	}

	public String deleteEmployee(@RequestBody Employee employe)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {

		csv = new CsvFileOperationHelper();
		String msg = "Sorry issue in deleting the employee, Employee Does not Exist in DB !!!";

		List<Employee> employeeList = csv.readFromCsvFile();

		if (!(employeeList.isEmpty() || employeeList.size() == 0)) {

			try {
				int index = employeeList.indexOf(employe);

				if (employeeList.remove(index) != null) {
					csv.insertToCsvFile(employeeList);
					msg = "Successfully deleted the employee";
				}
			} catch (ArrayIndexOutOfBoundsException ex) {
				System.out.println(ex.getMessage());
			}
		}

		return msg;

	}

	public List<Employee> findEmployeeByName(String employeeName) {
		System.out.println("emp name " + employeeName);
		List<Employee> employeeList = csv.readFromCsvFile();
		List<Employee> employeeReturnList = new ArrayList<>();

		if (!(employeeList.isEmpty() && employeeList.size() == 0)) {

			employeeReturnList = employeeList.stream().filter(e -> e.getEmpName().equals(employeeName))
					.collect(Collectors.toList());
		}
		return employeeReturnList;
	}

	public Employee findEmployeeById(long employeeId) {
		Employee employee = new Employee();
		List<Employee> employeeList = csv.readFromCsvFile();
		if (!(employeeList.isEmpty() && employeeList.size() == 0)) {
			employee = employeeList.stream().filter(e -> e.getEmpId() == employeeId).findFirst()
					.orElseThrow(() -> new IllegalArgumentException("This Employee does not exist in Db "));
		}
		return employee;
	}

}

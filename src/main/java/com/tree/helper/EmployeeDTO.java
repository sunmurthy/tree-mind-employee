package com.tree.helper;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tree.beans.Employee;

@XmlRootElement(name="employees")
public class EmployeeDTO {
	
	
	private List<Employee> employeeList;

	@XmlElement(name = "employee")
	@JsonProperty("employees")
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}
	
	
	
	
	

}

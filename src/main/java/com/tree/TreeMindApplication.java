package com.tree;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TreeMindApplication {

	public static void main(String[] args) {
		SpringApplication.run(TreeMindApplication.class, args);
	}
}
